require('dotenv').config();
const express = require('express');
const body_parser = require('body-parser');
const app = express();
const port = process.env.PORT || 3000;
//const URL_BASE = '/techu/v1/';
//const userFile = require('./user.json');

const request_json = require('request-json');
const URL_MLAB = 'https://api.mlab.com/api/1/databases/techu12db/collections/';
const URL_BASE = '/techu/v2/';
const apiKey_mlab = 'apiKey=' + process.env.API_KEY_MLAB;


app.listen(port, function(){
  console.log('Node JS escuchando en el puerto' + port);
});

app.use(body_parser.json());

///operación GET con mlab
///app.get('/holamundos',
///primer ejemplo
app.get(URL_BASE + 'users1',
      function(request,response){
      const http_client = request_json.createClient(URL_MLAB);
      console.log('Cliente HTTP a mLab creado correctamente');
      let field_param = 'f={"_id":0}&';
      http_client.get('users?' + field_param + apiKey_mlab,
      function(err,res,body){
        response.send(body);
      });
});

//segundo ejemplo
app.get(URL_BASE + 'users',
      function(req,res){
      const http_client = request_json.createClient(URL_MLAB);
      console.log('Cliente HTTP a mLab creado correctamente');

      let field_param = 'f={"_id":0}&';
      http_client.get('users?' + field_param + apiKey_mlab,
      function(err,respuestaMlab,body){
        console.log('error:' + err);
        console.log('Respuesta MLab:' + respuestaMlab);
        console.log('Body:'+ body);
        var response={};
        if(err){
          response = {"msg":"Error al recuperar users de mLab."};
          res.status(500);
        }else{
          if(body.length > 0){
            response = body;
          }else{
            response = {"msg":"usuario no encontrado"};
            res.status(404);
          }
        }
        res.send(response);
      });
});


app.get(URL_BASE + 'users/:id',
      function(req,res){

      console.log('GET /api/2/users/:id');
      console.log(req.params.id);
      let id = req.params.id;
      let field_param = 'f={"_id":0}&';
      let queryString ='q={"id_User":' + id + '}&';
      const http_client = request_json.createClient(URL_MLAB);
      http_client.get('users?' + queryString + apiKey_mlab,
        function(err,respuestaMlab,body){
          let response = {};
          if(err){
            response = {"msg":"Error en la petición mLab."};
            res.status(500);
          }else{
            if(body.length > 0){
              response = body;
            }else{
              response = {"msg":"usuario no encontrado"};
              res.status(404);
            }
          }
          res.send(response);
        });
});

app.get(URL_BASE + 'users/:id/accounts',
function(req,res){
  console.log('GET users/:id/accounts');
  let id = req.params.id;
  let field_param = 'f={"_id":0}&';
  let queryString =`q={"userID": ${id}}&`;
  const http_client = request_json.createClient(URL_MLAB);
  http_client.get('account?' + queryString + field_param + apiKey_mlab,
    function(err,respuestaMlab,body){
      let response = {};
      console.log('respuestaMlab:' + respuestaMlab);
      console.log(`body:${JSON.stringify(respuestaMlab)}`);
      if(err){
        response = {"msg":"Error en la petición mLab."};
        res.status(500);
      }else{
        if(body.length > 0){
          response = body;
        }else{
          response = {"msg":"usuario no encontrado"};
          res.status(404);
        }
      }
      res.send(response);
    });
});

app.post(URL_BASE + 'users',
  function(req,res){
    var clienteMlab = request_json.createClient(URL_MLAB);
    console.log(req.body);
    clienteMlab.get('users?' + apiKey_mlab,
    function(error,respuestaMlab,body){
      let newID = body.length +1;
      console.log("newID: " + newID);
      var newUser = {
        "id_User": newID + 1,
        "first_name": req.body.first_name,
        "last_name": req.body.last_name,
        "email": req.body.email,
        "password": req.body.password
      };
    clienteMlab.post(URL_MLAB + "users?"+ apiKey_mlab,newUser,
    function(error,respuestaMlab,body){
      res.send(body);
    });
  });
});


app.put(URL_BASE + 'users/:id',
  function (req, res){
    var clienteMlab = request_json.createClient(URL_MLAB);
    clienteMlab.get('users?' + apiKey_mlab,
      function(error,respuestaMlab,body){

      let newID = body.length + 1;
      console.log("newID: " + newID);
        var cambio = '{"$set":' + JSON.stringify(req.body)+'}';
        clienteMlab.put(URL_MLAB + 'users?q={"id_User": ' + newID + '}&' + apiKey_mlab , JSON.parse(cambio),
          function(error,respuestaMlab,body){
            console.log("body:" + body);
            res.send(body);
          });
      });
});

app.delete(URL_BASE +  'users/:id',
  function (request, res){
    console.log("entra al delete");
    console.log("request.params.id: " +  request.params.id);
    var id=request.params.id;
    var queryStringID = 'q={"id_User": ' + id + '}&';
    console.log(URL_MLAB + 'users?' + queryStringID + apiKey_mlab);
    var httpClient = request_json.createClient(URL_MLAB);
    httpClient.get('users?' + queryStringID + apiKey_mlab,
      function(error,respuestaMlab,body){
        var respuesta = body[0];
        console.log("body delete: " + respuesta);
        httpClient.delete(URL_MLAB + "users/" +  respuesta._id.$oid + '?' + apiKey_mlab,
          function(error,respuestaMlab,body){
            res.send(body);
          });
      });
});

//login
app.post(URL_BASE + 'users/login',
  function (request, res){
    console.log("POST /colapi/v2/login");
    var email = request.body.email;
    var password = request.body.password;
    var queryStringEmail = 'q={"email": "' + email + '"}&';
    var queryStringPass = 'q={password": "' + password + '}&';
    var clienteMlab = request_json.createClient(URL_MLAB);
    console.log("email:. " + email);
    console.log("password: " +  password);
    clienteMlab.get('users?' + queryStringEmail + queryStringPass + apiKey_mlab,
    function(error,respuestaMlab,body){
      console.log("entro al body :" + body);
      console.log("users?" + queryStringEmail + queryStringPass + apiKey_mlab)
      var respuesta =  body[0];
      console.log(respuesta);
        if (respuesta!=undefined){
            if(respuesta.password==password){
              console.log("login correcto");
              var session = {"logged":true};
              var login = '{"$set":' + JSON.stringify(session) + '}';
              console.log(URL_MLAB + '?q={"id_User": ' + respuesta.id_User+ '}&' + apiKey_mlab);
              clienteMlab.put('users?q={"id_User": ' + respuesta.id_User+ '}&' + apiKey_mlab,JSON.parse(login),
              function(error,respuestaMlab,body){
                res.send(respuesta);
              });
            }
          else{
            res.send({"msg":"contraseña incorrecta"});
          }
        }else{
          console.log("email incorrecto");
          res.send({"msg":"email incorrecto"});
        }
    });
  });

//logout
  app.post(URL_BASE + 'users/logout',
    function (request,response){
      var email = request.body.email;
      var queryStringEmail = 'q={"email": "' + email + '"}&';
      var clienteMlab = request_json.createClient(URL_MLAB);
      clienteMlab.get('users?' + queryStringEmail + apiKey_mlab,
      function(error,respuestaMlab,body){
        var respuesta = body[0];
        if(respuesta!=undefined){
          console.log("users?" + queryStringEmail + apiKey_mlab);
          console.log("respuesta:" +  respuesta);
            var session = {"logged":true};
            var logout = '{"$unset":' + JSON.stringify(session) + '}';
            clienteMlab.put('users?q={"id_User": ' + respuesta.id_User + '}&' + apiKey_mlab,JSON.parse(logout),
            function(err,resMlab,bod){
            response.send(respuesta);
          } );

        }else{
          res.send({"msg":"Error en logout"});

        }
      } );
    } );
